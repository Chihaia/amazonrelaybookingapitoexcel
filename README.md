A simple project where using Java and Selenium a extraction was made from Amazon Relay Booking Trips and parsed to a Excel file for Data Analyst.
URL: https://relay.amazon.co.uk/loadboard/search

For the login part was used os environment variables for security reason.

Modules used: 
- selenium-java 3.141.5
- rest-assured 4.2.0
- junit 4.12
- jackson-dataformat-csv 2.9.8
- poi-ooxml 3.9
