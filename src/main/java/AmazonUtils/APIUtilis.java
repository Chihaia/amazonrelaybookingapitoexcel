package AmazonUtils;

import AmazonTests.AmazonRelayScrappingPage;
import io.cucumber.core.gherkin.vintage.internal.gherkin.deps.com.google.gson.JsonArray;
import io.cucumber.core.gherkin.vintage.internal.gherkin.deps.com.google.gson.JsonObject;
import io.cucumber.core.gherkin.vintage.internal.gherkin.deps.com.google.gson.JsonParser;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class APIUtilis {

    private static final String userName = System.getenv("EMAIL");
    private static final String password = System.getenv("PASSWORD");
    public static JsonArray allTrips = new JsonArray();
    public static WebDriverWait wait = new WebDriverWait(AmazonRelayScrappingPage.driver, 30);

    public static void buildAllTheTripPages() throws InterruptedException {

        // login with env variables credentials
        String url = "https://relay.amazon.co.uk/loadboard/search";
        AmazonRelayScrappingPage.driver.get(url);
        WebElement userNameElem = AmazonRelayScrappingPage.driver.findElement(By.id("ap_email"));
        WebElement passwordElem = AmazonRelayScrappingPage.driver.findElement(By.id("ap_password"));
        WebElement signInButton = AmazonRelayScrappingPage.driver.findElement(By.id("signInSubmit"));
        userNameElem.click();
        userNameElem.sendKeys(userName);
        passwordElem.click();
        passwordElem.sendKeys(password);
        signInButton.click();
        Thread.sleep(1000);
        // identify the last page number to iterate in all pages
        WebElement lastPageNumber;
        lastPageNumber = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#base-container-body > div > div:nth-child(3) > div.css-93d7hd > nav > div > button:nth-child(8) > div.css-3p412q")));
        String pageNumberStr = lastPageNumber.getText().trim();
        System.out.println(pageNumberStr);
        int pageNumberInt = Integer.parseInt(pageNumberStr);
        int pageNumberDividedByTwo = pageNumberInt / 2;
        int pageNumbersParamsQuery = pageNumberDividedByTwo * 100;
        int counter_pages = 0;
        // scrap and build the final allTrips JsonArray with all the information from bookers
        while (counter_pages <= pageNumbersParamsQuery) {
            AmazonRelayScrappingPage.driver.get("https://relay.amazon.co.uk/api/loadboard?workOpportunityTypeList=ONE_WAY&trailerStatusFilters=&equipmentTypeFilters=&equipmentTypeFiltersForTags=&driverTypeFilters=&workOpportunityOperatingRegionFilter=&loadingTypeFilters=&sortByField=startTime&sortOrder=asc&nextItemToken=" + counter_pages + "&resultSize=200&searchURL=&isAutoRefreshCall=false&notificationId=&auditContextMap={%22rlbChannel%22:%22EXACT_MATCH%22,%22isOriginCityLive%22:%22false%22,%22isDestinationCityLive%22:%22false%22}");
            if ((counter_pages + 100) == pageNumbersParamsQuery) {
                AmazonRelayScrappingPage.driver.get("https://relay.amazon.co.uk/api/loadboard?workOpportunityTypeList=ONE_WAY&trailerStatusFilters=&equipmentTypeFilters=&equipmentTypeFiltersForTags=&driverTypeFilters=&workOpportunityOperatingRegionFilter=&loadingTypeFilters=&sortByField=startTime&sortOrder=asc&nextItemToken=" + pageNumbersParamsQuery + "&resultSize=200&searchURL=&isAutoRefreshCall=false&notificationId=&auditContextMap={%22rlbChannel%22:%22EXACT_MATCH%22,%22isOriginCityLive%22:%22false%22,%22isDestinationCityLive%22:%22false%22}");
            }
            String text = AmazonRelayScrappingPage.driver.findElement(By.xpath("/html/body/pre")).getText();
            JsonObject jsonObject = new JsonParser().parse(text).getAsJsonObject();
            JsonArray workOpportunities = jsonObject.get("workOpportunities").getAsJsonArray();
            allTrips.addAll(workOpportunities);
            counter_pages = counter_pages + 200;
        }
    }

    public static void parseAllTheTripsToExcelDocument() throws IOException, InterruptedException {

        String idStartAndEndTripString;
        String idCostAndDistance;
        String secondIdCostAndDistance;
        String uniqueId;

        String StartTripCollectionData;
        String EndTripCollectionData;
        String DistanceCollectionData;
        String CostCollectionData;

        //Creating the excel file
        //Create blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet spreadsheet = workbook.createSheet(" Amazon Relay All Bookings ");

        //Create row object
        XSSFRow row;

        Map<String, Object[]> amazonRelayInfo = new TreeMap<String, Object[]>();
        amazonRelayInfo.put("1", new Object[]{
                "idTrip",
                "StartCity",
                "StartLine1",
                "StartCountry",
                "StartLatitude",
                "StartLongitude",
                "EndCity",
                "EndLine1",
                "EndCountry",
                "EndLatitude",
                "EndLongitude",
                "Payout",
                "Distance_KM"});

        for (int i = 0; i < allTrips.size(); i++) {
            int rowIterator = i + 2;
            String rowNumber = Integer.toString(rowIterator);

            uniqueId = allTrips.get(i).toString();
            JsonObject idObj = new JsonParser().parse(uniqueId).getAsJsonObject();

            idStartAndEndTripString = allTrips.get(i).toString();
            JsonObject startLocationObj = new JsonParser().parse(idStartAndEndTripString).getAsJsonObject();

            StartTripCollectionData = startLocationObj.get("startLocation").toString();
            JsonObject startTripCollectionData = new JsonParser().parse(StartTripCollectionData).getAsJsonObject();

            EndTripCollectionData = startLocationObj.get("endLocation").toString();
            JsonObject endTripCollectionData = new JsonParser().parse(EndTripCollectionData).getAsJsonObject();

            idCostAndDistance = allTrips.get(i).toString();
            JsonObject costAndDistanceOjb = new JsonParser().parse(idCostAndDistance).getAsJsonObject();
            JsonArray costAndDistance = costAndDistanceOjb.get("loads").getAsJsonArray();

            secondIdCostAndDistance = costAndDistance.get(0).toString();
            JsonObject secondCostAndDistanceObj = new JsonParser().parse(secondIdCostAndDistance).getAsJsonObject();

            DistanceCollectionData = secondCostAndDistanceObj.get("distance").toString();
            JsonObject DistanceValue = new JsonParser().parse(DistanceCollectionData).getAsJsonObject();
            CostCollectionData = secondCostAndDistanceObj.get("payout").toString();
            JsonObject CostValue = new JsonParser().parse(CostCollectionData).getAsJsonObject();


            amazonRelayInfo.put(rowNumber, new Object[]{
                    idObj.get("id").toString(),
                    startTripCollectionData.get("city").toString(),
                    startTripCollectionData.get("line1").toString(),
                    startTripCollectionData.get("country").toString(),
                    startTripCollectionData.get("latitude").toString(),
                    startTripCollectionData.get("longitude").toString(),
                    endTripCollectionData.get("city").toString(),
                    endTripCollectionData.get("line1").toString(),
                    endTripCollectionData.get("country").toString(),
                    endTripCollectionData.get("latitude").toString(),
                    endTripCollectionData.get("longitude").toString(),
                    CostValue.get("value").toString(),
                    DistanceValue.get("value").toString()});

        }
        //Iterate over data and write to sheet
        Set<String> keyid = amazonRelayInfo.keySet();
        int rowid = 0;

        for (String key : keyid) {
            row = spreadsheet.createRow(rowid++);
            Object[] objectArr = amazonRelayInfo.get(key);
            int cellid = 0;

            for (Object obj : objectArr) {
                Cell cell = row.createCell(cellid++);
                cell.setCellValue((String) obj);
            }
        }
        //Write the workbook in file system
        FileOutputStream out = new FileOutputStream(
                new File("C:\\Users\\NewClassCargo\\OrderList.xlsx"));
        workbook.write(out);
        out.close();
        System.out.println("OrderList.xlsx written successfully");

    }
}
