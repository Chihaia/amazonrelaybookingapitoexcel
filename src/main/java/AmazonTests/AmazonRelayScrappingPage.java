package AmazonTests;

import AmazonUtils.APIUtilis;
import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;


public class AmazonRelayScrappingPage {
    public static WebDriver driver = new ChromeDriver();
    @Rule
    public TestName name = new TestName();

    public static SoftAssertions softly = new SoftAssertions();

    @Before
    public void initBeforeTest() {
        System.out.println(name.getMethodName());
    }

    @After
    public void clean() {
        System.out.println(name.getMethodName());
        softly.assertAll();
        driver.close();
    }

    @Test
    public void searchRelay() throws InterruptedException, IOException {
        APIUtilis.buildAllTheTripPages();
        APIUtilis.parseAllTheTripsToExcelDocument();
    }
}

